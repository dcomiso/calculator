package edu.hawaii.ics211;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * CalculatorTest.java
 * Tests the calculator class methods.
 * @author Derrick Comiso
 *
 */
public class CalculatorTest extends Calculator {
	
	/**
	 * Tests the add method.
	 */
	@Test
	public void testAdd() {
		int answer = 2;
		int actual = Calculator.add(1, 1);
		assertEquals("adding 1 and 1", answer, actual);
	}
	
	/**
	 * Tests the subtract method.
	 */
	@Test
	public void testSubtract() {
		int answer = 1;
		int actual = Calculator.subtract(2, 1);
		assertEquals("subtracting 1 from 2", answer, actual);
	}
	
	/**
	 * Tests the multiply method.
	 */
	@Test
	public void testMultiply() {
		int answer = 4;
		int actual = Calculator.multiply(2, 2);
		assertEquals("multiplying 2 and 2", answer, actual);
	}

	/**
	 * Tests the divide method.
	 */
	@Test
	public void testDivide() {
		int answer = 2;
		int actual = Calculator.divide(4, 2);
		assertEquals("Dividing 4 by 2", answer, actual);
	}
	
	/**
	 * Tests the divide method by dividing by 0.
	 */
	@Test
	public void testDivideByZero() {
		try {
			Calculator.divide(10, 0);
			fail("Should throw exception");
		}
		catch (ArithmeticException e) {
			
		}
	}
	
	/**
	 * Tests the modulo method.
	 */
	@Test
	public void testModulo() {
		int answer = 1;
		int actual = Calculator.modulo(5, 2);
		assertEquals("Remainder of 5 divided by 2", answer, actual);
	}
	
	/**
	 * Tests the power method.
	 */
	@Test
	public void testPower() {
		int answer = 4;
		int actual = Calculator.pow(2, 2);
		assertEquals("Power of 2 to 2", answer, actual);
	}
	
	/**
	 * Tests the Power method by using a negative integer.
	 */
	@Test
	public void testNegPower() {
		int answer = -8;
		int actual = Calculator.pow(-2, 3);
		assertEquals("Power of -2 to 3", answer, actual);
	}
}
